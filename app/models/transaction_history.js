const db = require('./base_models/'),
  Sequelize = require('sequelize'),
  accountModel = require('./account'),
  Op = Sequelize.Op,
  ATIVE = true,
  PASSIVE = false;


class TransactionHistory {

  constructor() {

  }

  getTransactionHistoryByAccountId(accountId) {

    return db.transaction_history.findAll({
      where: {
        [Op.or]: [{to: accountId}, {from: accountId}]
      }
    })
  }

  makeTransaction(params) {

    let transaction = {
      from: params.form,
      to: params.to,
      type: params.type,
      amount: Math.abs(parseInt(params.amount, 10))
    }

    return db.sequelize.transaction({autocommit: false})
      .then((t) => {
      let transaction;
     return db.transaction_history.create(params, {transaction: t})
        .then((row) => {
          return row;
        })
        .then((row) => {
          transaction = row;
          return accountModel.getActiveAccountInfo()
        })
        .then((account) => {
          account.amount += this.getAmount(params.type, params.amount, ATIVE);
          return account.save({transaction: t})
        })
        .then((account) => {

          if (account.amount <= 0) {
            throw new Error('negative balance');
          }

        })
        .then(() => {
          return accountModel.getPassiveAccountInfo()
        })
        .then((account) => {
          account.amount += this.getAmount(params.type, params.amount, PASSIVE);
          return account.save({transaction: t})
        })
        .then((account) => {
          if (account.amount <= 0) {
            throw new Error('negative balance');
          }

          return t.commit();
        })
        .then(() => {
          return transaction;
        })
        .catch(error => {
          t.rollback();
          throw error;
        })
    })
      .catch(err => {
        throw err;
      })
  }

  getAmount(type, amount, active) {

    if (active) {
      return parseInt(((type === 'credit') ? -amount : amount), 10);
    }

    return parseInt(((type === 'debit') ? -amount : amount), 10);
  }

}


module.exports = new TransactionHistory();