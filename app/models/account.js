const db = require('./base_models/');

const ACTIVE_ACCOUNT_STATE = 1;
const PASSIVE_ACCOUNT_STATE = 0;

class Account {

  constructor() {

  }

  getAccountInfo(type){

    return db.account.find({
      where: {
        active: type
      }
    }).then((user) => {
      if (!user) {
        return {};
      }

      return user;
    });

  }

  getPassiveAccountInfo(){
    return this.getAccountInfo(PASSIVE_ACCOUNT_STATE)
  }

  getActiveAccountInfo() {
    return this.getAccountInfo(ACTIVE_ACCOUNT_STATE)
  }
}


module.exports = new Account();