var db = require('./../models')

module.exports = function(app) {

  app.use('/account', require('./account/account'))
  app.use('/history', require('./transaction_history/transaction-history'))

};
