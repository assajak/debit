var express = require('express'),
  router = express.Router(),
  db = require('./../../models');

router.get('/', function(req, res) {

  db.account.getActiveAccountInfo().then((data) => {
    res.json(data)
  })

})

router.get('/passive', function(req, res) {

  db.account.getPassiveAccountInfo().then((data) => {
    res.json(data)
  })

})


module.exports = router;