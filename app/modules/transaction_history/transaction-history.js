var express = require('express'),
  router = express.Router(),
  db = require('./../../models');


router.get('/user/:id', (req, res) => {
  let id = req.params.id || 1;

  db.transaction_history.getTransactionHistoryByAccountId(id).then((info) => {
    res.json(info);
  });

});

router.options('/', (req, res) => {
  res.status(200).json()
})

router.post('/', (req, res) => {
  let params = req.body;

  db.transaction_history.makeTransaction(params).then((t) => {
    res.json(t);
  })
    .catch((err) => {
      res.status(403).json({error: err.toString()})
  })

})


module.exports = router;