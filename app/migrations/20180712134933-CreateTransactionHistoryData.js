'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.sequelize.query("SELECT id FROM  `account` WHERE `active` = '1'").then((activeAccount) => {
      return activeAccount[0][0];
    }).then((activeAccount) => {
      return queryInterface.sequelize.query("SELECT id FROM  `account` WHERE `active` = '0'").then((pasiveAccount) => {
        return {active: activeAccount, passive: pasiveAccount[0][0]}
      })
    }).then((data) => {

      console.log(JSON.stringify(data), 'data')

      return queryInterface.bulkInsert('transaction_history', [
        {
          to: data.active.id,
          from: data.passive.id,
          amount: 100,
          type: 'credit',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          to: data.passive.id,
          from: data.active.id,
          amount: 75,
          type: 'debit',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ], {});
    })

  },

  down: (queryInterface, Sequelize) => {

  }
};
