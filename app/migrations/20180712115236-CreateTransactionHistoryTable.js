'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.createTable(
      'transaction_history',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        from: {
          type: Sequelize.INTEGER,
          references: {
            model: 'account',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        to: {
          type: Sequelize.INTEGER,
          references: {
            model: 'account',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        amount: {
          type: Sequelize.FLOAT,
        },
        type: {
          type: Sequelize.ENUM('credit', 'debit'),
          allowNull: false
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      },
      {
        engine: 'InnoDB',
        charset: 'utf8'
      }
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('transaction_history');
  }
};
