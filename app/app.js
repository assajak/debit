
let express = require('express');
let path = require('path');
let bodyParser = require('body-parser')
let app = express();
let router = express.Router();
let port = 5000;


//body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "content-type");
  next();
});

require('./modules/routes')(app);


app.get('*', (req, res) => {
  res.render('./404');
});



app.listen(port, (err) =>  {
  console.log('running server on port ' + port);
});
