export default {
  getUrl(url){
    return location.protocol+'//'+location.host.split(':')[0]+':5000/'+url;
  },
  setTransactionListToLocalStorage(list){
    return localStorage.setItem('transaction', JSON.stringify(list))
  },
  getTransactionListToLocalStorage(){
    return JSON.parse(localStorage.getItem('transaction')) || []
  },
  addTransactionToLocalStorage(transaction){
    let list = this.getTransactionListToLocalStorage();
    list.push(transaction);
    this.setTransactionListToLocalStorage(list);

    return list;
  }
}
