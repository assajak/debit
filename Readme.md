# Backend setup

> required

```
1. mysql server
2. nodejs
```
# Instriction

```
Setup global modules:
- npm install --save sequelize-cli -g
- npm install -g sequelize-auto
- npm install -g mysql
- npm i -g sequelize-auto
```

```
run next command from ./app/
- cp config.json.sample config.json
- modify config with your credentials to database
-  run "sequelize db:migrate" for update migrations
- updete comand (set your credentials to db here and run it) sequelize-auto -h 127.0.0.1 -d debet -u p_debet -x some_random_password -p 3306  --dialect mysql -c ./config/config.json -o ./models/base_models/
- npm i
- run node app.js
```

# Frontend setup
```
  run next command from ./frontnd
  - npm i
  - npm run dev
```
